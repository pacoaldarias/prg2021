/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta3;

/**
 *
 * @author paco
 */
public class pregunta3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int  matriz[][] = new int[3][3];
        rellenar(matriz);
        mostrar(matriz);
        filas(matriz);
        
    }

    private static void rellenar(int[][] matriz) {
        for (int i =0; i < matriz.length ; i++) 
            for (int  j=0; j < matriz[i].length; j++)
                matriz[i][j]= (int) (Math.random() * (100+1));
    }

    private static void mostrar(int[][] matriz) {
        for (int i =0; i < matriz.length ; i++) {
            for (int  j=0; j < matriz[i].length; j++) {
                System.out.format("%3d ",matriz[i][j]);
            }
        System.out.println("");
        }
    }

    private static void filas(int[][] matriz) {
        for (int i =0; i < matriz.length ; i++) {
            int suma=0;  
            for (int  j=0; j < matriz[i].length; j++) {
                suma = suma + matriz[i][j];
            }
            if (suma > 10) {
            System.out.format("La fila "+i+ " suma mas de 10\n");
            }
    }  
}
    
}
