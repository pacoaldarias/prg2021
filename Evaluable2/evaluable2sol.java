/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluable2;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author indir
 */
public class evaluable2 {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String palabras [] = {"ARBOL", "ESTRELLA","BELEN","DECORACION","RENO","REGALO","ROJO","VERDE","TURRON","TRINEO","CAMELLO","PAJE","ELFO","NAVIDAD","POLVORON"};
        String orientaciones [] = {"Vertical", "Horizontal"};
        char sopa [][]= new char [15][15];
        char solucion [][] = new char [15][15];
        
        System.out.println("Sopa de letras");
        System.out.println(' ');

        for(int i = 0; i < sopa.length; i++) {
            Arrays.fill(sopa[i], '.');
        }
        
        for (int i = 0; i < palabras.length; i++) {
            String palabra = palabras[i];
            boolean intentar = true;
            int indiceorientacion = (int) (Math.random() * orientaciones.length);
            String orientacion = orientaciones[indiceorientacion];
            int filamax = sopa.length;
            int columnamax = sopa[0].length;
            boolean esHorizontal = "Horizontal".equals(orientacion);
            if (esHorizontal) {
                columnamax = columnamax - palabra.length();
            } else {
                filamax = filamax - palabra.length();
            }
            int fila = (int) (Math.random() * filamax);
            int columna = (int) (Math.random() * columnamax);
            boolean solapa = false;
            char letras [] = palabra.toCharArray();

            for (int j = 0; j < letras.length; j++) {
                if (esHorizontal) {
                    if (sopa[fila][columna + j] != '.') {
                       solapa = true;
                    }
                } else {
                    if (sopa[fila + j][columna] != '.') {
                       solapa = true;
                    }
                }
            }

            if (solapa == false) {
                for (int indiceLetra = 0; indiceLetra < letras.length; indiceLetra++) {
                    if (esHorizontal) {
                        sopa[fila][columna + indiceLetra] = letras[indiceLetra];
                    } else {
                        sopa[fila + indiceLetra][columna] = letras[indiceLetra];
                    }
                }
            } else {
                i--;
            }
        }

        for (int i = 0; i < palabras.length; i++) {
            System.out.println(i + 1 + ". " + palabras[i]); 
        }
        System.out.println(' ');

        for (int i = 0; i < sopa.length; i++) {
            for (int j = 0; j < sopa[i].length; j++) {
                solucion[i][j] = sopa[i][j];
                if (sopa[i][j] == '.') {
                    int valorASCII = (int) (Math.random() * ((int) 'Z' - (int) 'A') + ((int) 'A'));
                    char letra = (char) valorASCII;
                    sopa[i][j] = letra;
                }
                System.out.print(sopa[i][j] + " ");
            }
            System.out.println(' ');  
        }

        System.out.println(' ');
        Scanner in = new Scanner(System.in);
        System.out.print("Para ver la solución, escribre SI: ");
        String s = in.nextLine();
        System.out.println(' ');
        if ("SI".equals(s)) {
            for (int i = 0; i < solucion.length; i++) {
                for (int j = 0; j < solucion[i].length; j++) {
                    System.out.print(solucion[i][j] + " ");
                }
                System.out.println(' ');
            }
        }
    }
}
