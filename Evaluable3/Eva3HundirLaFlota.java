package prg_trabajo2_hundirlaflota;

import java.util.Arrays;
import java.util.Scanner;

public class PRG_Trabajo2_HundirLaFlota {

    //=======================================
    // FUNCIÓN MAIN & MENÚ PRINCIPAL
    //=======================================
    public static void main(String[] args) {

        System.out.println("===== BIENVENIDO A HUNDIR LA FLOTA =====");

        int opcion = menu_modo_juego();

        switch (opcion) {
            case 1:
                juega_partida(10, 10, 5, 3, 1, 1, 50);
                break;
            case 2:
                juega_partida(10, 10, 2, 1, 1, 1, 30);
                break;
            case 3:
                juega_partida(10, 10, 1, 1, 0, 0, 10);
                break;
            case 4:
                int p[] = pide_parametros_partida_personalizada();
                juega_partida(p[0], p[1], p[2], p[3], p[4], p[5], p[6]);
                break;
            default:
                System.err.println("Opción incorrecta");
        }
    }

    // Muestra el menú y devuelve la opción elegida por el usuario (1, 2, 3 o 4)
    public static int menu_modo_juego() {

        int opcion = 0;
        boolean ok = false;

        System.out.println("Niveles de dificultad:");
        System.out.println("1. Facil: 5 lanchas, 3 buques, 1 acorazado y 1 portaaviones (50 disparos).");
        System.out.println("2. Medio: 2 lanchas, 1 buque, 1 acorazado y 1 portaaviones (30 disparos).");
        System.out.println("3. Difícil: 1 lancha y 1 buque (10 disparos).");
        System.out.println("4. Personalizado.");

        opcion = pide_int_forzoso_entre_min_y_max("¿Cual eliges? ", 1, 4);

        return opcion;
    }

    //=======================================
    // FUNCIÓNES DE JUGAR PARTIDA
    //=======================================
    //
    // Juega una nueva partida en un tablero de tamaño filas x columnas,
    // con los barcos indicados y el nº máximo de max_disparos
    public static void juega_partida(int filas, int columnas, int lanchas, int buques, int acorazados, int portaaviones, int max_disparos) {

        // Mensaje de inicio de partida
        System.out.println("");
        System.out.println("===== ¡JUEGUEMOS A HUNDIR LA FLOTA! =====");

        // Creamos tablero e insertamos los barcos
        char[][] tablero = crear_tablero(filas, columnas);
        insertar_barcos(tablero, lanchas, buques, acorazados, portaaviones);

        // Contador de turnos
        int turno = 0;

        // Jugamos mientras queden max_disparos y barcos
        while ((turno <= max_disparos) && quedan_barcos(tablero)) {

            // Mostramos ronda y tablero
            System.out.println("");
            System.out.println("***** RONDA " + turno + " *****");
            //muestra_tablero_completo(tablero);
            muestra_tablero_visible(tablero);

            // Pedimos coordenada de disparo
            int disparo[] = pide_coordenadas_disparo(tablero);
            int f = disparo[0];
            int c = disparo[1];

            // Realizamos disparo
            realiza_disparo(tablero, f, c);

            // Actualizamos turno
            turno++;
        }

        // Si terminó porque no quedaban turnos
        if (turno > max_disparos) {
            System.out.println("¡HAS PERDIDO! :(  (se acabaron los disparos)");

        } // Si terminó porque no quedan barcos
        else if (!quedan_barcos(tablero)) {
            System.out.println("¡HAS GANADO! :)  (hundiste todos los barcos)");
        }

        // Mostramos el tablero completo
        muestra_tablero_completo(tablero);

        // Mensaje de fin de partida
        System.out.println("===== FIN DE LA PARTIDA =====");
    }

    // Pide los param de una partida personalizada y los devuelve en un vector
    public static int[] pide_parametros_partida_personalizada() {
        
        // Vector donde guardaremos los param
        int[] param = new int[7];
        
        // Pedimos nº de filas y columnas
        param[0] = pide_int_forzoso_entre_min_y_max("¿Nº de filas? ", 1, 99);
        param[1] = pide_int_forzoso_entre_min_y_max("¿Nº de columnas? ", 1, 99);
        
        // Pedimos nº de lanchas
        param[2] = pide_int_forzoso_entre_min_y_max("¿Nº de lanchas? ", 0, 99);
        param[3] = pide_int_forzoso_entre_min_y_max("¿Nº de buques? ", 0, 99);
        param[4] = pide_int_forzoso_entre_min_y_max("¿Nº de acorazados? ", 0, 99);
        param[5] = pide_int_forzoso_entre_min_y_max("¿Nº de portaaviones? ", 0, 99);
        
        // Pedimos nº de intentos
        param[6] = pide_int_forzoso_entre_min_y_max("¿Nº de intentos? ", 1, 1000);
        
        // Devolvemos los parámetros
        return param;
    }

    //=======================================
    // FUNCIONES DE TABLERO
    //=======================================
    //
    // Crea y devuelve un tablero vacío (sin barcos) de tamaño filas x columnas
    // Contendrá '-' en todas sus posiciones
    public static char[][] crear_tablero(int filas, int columnas) {
        // Creamos la matriz
        char[][] tablero = new char[filas][columnas];
        // Rellena todas las filas con el char '-'
        for (int i = 0; i < tablero.length; i++) {
            Arrays.fill(tablero[i], '-');
        }
        // Devuelve el tablero
        return tablero;
    }

    // Muestra por pantalla el tablero completo (lo visible y lo oculto)
    public static void muestra_tablero_completo(char[][] tablero) {

        // Mostramos la cabecera con los números de las columnas
        System.out.print("  ");
        for (int i = 0; i < tablero[0].length; i++) {
            System.out.printf("%2s ", i);
        }
        System.out.println("");

        // Para cada fila
        char letra = 'A';
        for (int i = 0; i < tablero.length; i++) {

            // Mostramos la letra de la fila
            System.out.print(letra + " ");
            letra++;

            // Mostramos las columnas de la fila
            for (int j = 0; j < tablero[i].length; j++) {
                System.out.printf("%2s ", tablero[i][j]);
            }
            System.out.println("");
        }
    }

    // Muestra por pantalla el tablero visible al usuario
    public static void muestra_tablero_visible(char[][] tablero) {

        // Mostramos la cabecera con los números de las columnas
        System.out.print("  ");
        for (int i = 0; i < tablero[0].length; i++) {
            System.out.printf("%2s ", i);
        }
        System.out.println("");

        // Para cada fila
        char letra = 'A';
        for (int i = 0; i < tablero.length; i++) {

            // Mostramos la letra de la fila
            System.out.print(letra + " ");
            letra++;

            // Mostramos las columnas de la fila
            for (int j = 0; j < tablero[i].length; j++) {
                
                // Mostramos solo las casillas con 'A' o 'X'. El resto '-'
                if (tablero[i][j] == 'A' || tablero[i][j] == 'X') {
                    System.out.printf("%2s ", tablero[i][j]);
                } else {
                    System.out.printf("%2s ", "-");
                }
            }
            System.out.println("");
        }
    }

    //=======================================
    // FUNCIONES DE BARCOS
    //=======================================
    //
    // Comprueba si queda algún barco en el tablero
    public static boolean quedan_barcos(char[][] tablero) {
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[0].length; j++) {
                if (tablero[i][j] != '-' && tablero[i][j] != 'A' && tablero[i][j] != 'X') {
                    return true;
                }
            }
        }
        return false;
    }

    // Inserta todos los barcos indicados en posiciones aleatorias del tablero
    public static void insertar_barcos(char[][] tablero, int lanchas, int buques, int acorazados, int portaaviones) {
        // Insertamos los portaaviones
        for (int i = 1; i <= portaaviones; i++) {
            inserta_portaaviones(tablero);
        }
        // Insertamos los acorazados
        for (int i = 1; i <= acorazados; i++) {
            inserta_acorazado(tablero);
        }
        // Insertamos los buques
        for (int i = 1; i <= buques; i++) {
            inserta_buque(tablero);
        }
        // Insertamos las lanchas
        for (int i = 1; i <= lanchas; i++) {
            inserta_lancha(tablero);
        }
    }

    // Inserta una lancha en una posición aleatoria del tablero
    public static void inserta_lancha(char[][] tablero) {
        int max_intentos = 100;
        do {
            // Coordenada aleatoria
            int coord[] = coordenada_aleatoria(tablero);
            int f = coord[0];
            int c = coord[1];

            // Si cabe en esa coordenada
            if (comprueba_inserta_lancha(tablero, f, c)) {

                // Insertamos lancha
                tablero[f][c] = 'L';

                // Termina la función
                return;
            }

            max_intentos--;

        } while (max_intentos > 0);

        // Si llega aquí, lo hemos intentado 100 veces y no ha sido posible
        System.err.println("ERROR: No ha sido posible insertar la lancha.");
    }

    // Inserta un buque en una posición aleatoria del tablero (si cabe)
    public static void inserta_buque(char[][] tablero) {
        int max_intentos = 100;
        do {
            // Coordenada aleatoria
            int coord[] = coordenada_aleatoria(tablero);
            int f = coord[0];
            int c = coord[1];

            // Si cabe en esa coordenada
            if (comprueba_inserta_buque(tablero, f, c)) {

                // Insertamos buque
                tablero[f][c] = 'B';
                tablero[f][c + 1] = 'B';
                tablero[f][c + 2] = 'B';

                // Termina la función
                return;
            }

            max_intentos--;

        } while (max_intentos > 0);

        // Si llega aquí, lo hemos intentado 100 veces y no ha sido posible
        System.err.println("No ha sido posible insertar el buque.");
    }

    // Inserta un acorazado en una posición aleatoria del tablero (si cabe)
    public static void inserta_acorazado(char[][] tablero) {
        int max_intentos = 100;
        do {
            // Coordenada aleatoria
            int coord[] = coordenada_aleatoria(tablero);
            int f = coord[0];
            int c = coord[1];

            // Si cabe en esa coordenada
            if (comprueba_inserta_acorazado(tablero, f, c)) {

                // Insertamos acorazado
                tablero[f][c] = 'Z';
                tablero[f][c + 1] = 'Z';
                tablero[f][c + 2] = 'Z';
                tablero[f][c + 3] = 'Z';

                // Termina la función
                return;
            }

            max_intentos--;

        } while (max_intentos > 0);

        // Si llega aquí, lo hemos intentado 100 veces y no ha sido posible
        System.err.println("No ha sido posible insertar el acorazado.");
    }

    // Inserta un portaaviones en una posición aleatoria del tablero (si cabe)
    public static void inserta_portaaviones(char[][] tablero) {
        int max_intentos = 100;
        do {
            // Coordenada aleatoria
            int coord[] = coordenada_aleatoria(tablero);
            int f = coord[0];
            int c = coord[1];

            // Si cabe en esa coordenada
            if (comprueba_inserta_portaaviones(tablero, f, c)) {

                // Insertamos portaaviones
                tablero[f][c] = 'P';
                tablero[f + 1][c] = 'P';
                tablero[f + 2][c] = 'P';
                tablero[f + 3][c] = 'P';
                tablero[f + 4][c] = 'P';

                // Termina la función
                return;
            }

            max_intentos--;

        } while (max_intentos > 0);

        // Si llega aquí, lo hemos intentado 100 veces y no ha sido posible
        System.err.println("No ha sido posible insertar el portaaviones.");
    }

    // Comprueba si es posible insertar una lancha en la coordenada (f,c) del tablero
    public static boolean comprueba_inserta_lancha(char[][] tablero, int f, int c) {
        if (!existe_coordenada(tablero, f, c)) {
            return false;
        }
        return (tablero[f][c] == '-');
    }

    // Comprueba si es posible insertar un buque en la coordenada (f,c) del tablero
    public static boolean comprueba_inserta_buque(char[][] tablero, int f, int c) {
        if (!existe_coordenada(tablero, f, c) || !existe_coordenada(tablero, f, c + 1) || !existe_coordenada(tablero, f, c + 2)) {
            return false;
        }
        return (tablero[f][c] == '-' && tablero[f][c + 1] == '-' && tablero[f][c + 2] == '-');
    }

    // Comprueba si es posible insertar un acorazado en la coordenada (f,c) del tablero
    public static boolean comprueba_inserta_acorazado(char[][] tablero, int f, int c) {
        if (!existe_coordenada(tablero, f, c) || !existe_coordenada(tablero, f, c + 1) || !existe_coordenada(tablero, f, c + 2) || !existe_coordenada(tablero, f, c + 3)) {
            return false;
        }
        return (tablero[f][c] == '-' && tablero[f][c + 1] == '-' && tablero[f][c + 2] == '-' && tablero[f][c + 3] == '-');
    }

    // Comprueba si es posible insertar un portaaviones en la coordenada (f,c) del tablero
    public static boolean comprueba_inserta_portaaviones(char[][] tablero, int f, int c) {
        if (!existe_coordenada(tablero, f, c) || !existe_coordenada(tablero, f + 1, c) || !existe_coordenada(tablero, f + 2, c) || !existe_coordenada(tablero, f + 3, c) || !existe_coordenada(tablero, f + 4, c)) {
            return false;
        }
        return (tablero[f][c] == '-' && tablero[f + 1][c] == '-' && tablero[f + 2][c] == '-' && tablero[f + 3][c] == '-' && tablero[f + 4][c] == '-');
    }

    //=======================================
    // FUNCIONES DE DISPARO
    //=======================================
    //
    // Pide coordenadas de disparo (repite hasta que sean válidas)
    // y las devuelve en un vector de tamaño 2.
    public static int[] pide_coordenadas_disparo(char[][] tablero) {
        
        Scanner in = new Scanner(System.in);
        String coord;

        // Pedimos una y otra vez hasta que introduzca coordenadas correctas
        do {
            System.out.print("Introduce coordenadas de disparo (ejemplos: A0, D3, F8): ");

            // Recogemos coordenadas
            coord = in.next();
            
            // Si tiene menos de 2 caracteres, formato incorrecto
            if (coord.length() < 2) {
                System.out.println("Coordenadas incorrectas.");
                continue; // Saltamos a la siguiente iteración del while
            }
            
            // Fila
            String F = coord.substring(0, 1);
            //System.out.println(F);
            int f = fila_string_to_int(F);

            // Columna
            String C = coord.substring(1, coord.length());
            //System.out.println(C);
            int c = Integer.parseInt(C);

            // Si existe la coordenada, las devolvemos como vector y termina la función
            if (existe_coordenada(tablero, f, c)) {
                int[] coordenadas = {f, c};
                return coordenadas;
            }
            else {
                System.out.println("Coordenadas incorrectas.");
            }
            
        } while (true);
    }

    // Realiza un disparo en las coordenadas indicadas
    public static void realiza_disparo(char[][] tablero, int f, int c) {

        // Si no existen esas coordenadas, mensaje de error y salimos
        if (!existe_coordenada(tablero, f, c)) {
            System.out.println("ERROR: No existen las coordenadas " + f + " " + c);
            return;
        }

        // Si hay Agua
        if (tablero[f][c] == '-') {
            System.out.println("¡AGUA!");
            tablero[f][c] = 'A';
        } // Si hay un barco
        else if (tablero[f][c] == 'L' || tablero[f][c] == 'B' || tablero[f][c] == 'Z' || tablero[f][c] == 'P') {
            System.out.println("¡TOCADO!");
            tablero[f][c] = 'X';
        } // Si ya se disparó en esa coordenada
        else if (tablero[f][c] == 'A' || tablero[f][c] == 'X') {
            System.out.println("¡Ya disparaste antes en esa coordenada, zoquete!");
        }

    }

    // Transforma la coordenada de la fila en int (A es 0, B es 1, C es 2, etc.)
    // Devuelve nº negativo si la cadena está mal formateada
    public static int fila_string_to_int(String s) {

        // Si no es de tamaño 1, error
        if (s.length() != 1) {
            return -1;
        }

        // Lo pasamos a char
        char c = s.charAt(0);

        // Restamos 'A' y lo pasamos a int
        int f = c - 'A';
        return f;
    }

    //=======================================
    // FUNCIONES ÚTILES ADICIONALES
    //=======================================
    //
    // Devuelve un int int_aleatorio entre min y max
    public static int int_aleatorio(int min, int max) {
        return (int) (min + Math.random() * (max - min + 1));
    }

    // Devuelve una coordenada aleatoria del tablero (en un vector tamaño 2)
    public static int[] coordenada_aleatoria(char[][] tablero) {
        int f = int_aleatorio(0, tablero.length - 1);
        int c = int_aleatorio(0, tablero[0].length - 1);
        int[] coord = {f, c};
        return coord;
    }

    // Comprueba si existe la coordenada en el tablero
    public static boolean existe_coordenada(char[][] tablero, int f, int c) {
        return (f >= 0 && f < tablero.length && c >= 0 && c < tablero[0].length);
    }
    
    // Muestra el texto al usuario, le pide por teclado un valor int y lo devuelve.
    // Se repite indefinidamente hasta que el valor introducido esté entre min y max
    public static int pide_int_forzoso_entre_min_y_max(String texto, int min, int max) {
        Scanner in = new Scanner(System.in);
        int valor;
        do {
            System.out.print(texto);
            valor = in.nextInt();
            if (valor >= min && valor <= max) {
                return valor;
            }
            else {
                System.out.println("Valor no válido. Debe estar entre " + min + " y " + max + ".");
            }
        } while (true);
    }
    
}