/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta2;

import java.util.Scanner;

/**
 *
 * @author paco
 */
public class pregunta2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        String frase;
        char letra;
        Scanner entrada = new Scanner(System.in);
        
        System.out.print("Introduce frase: ");
        frase = entrada.nextLine();
        
        int vocales = 0;
        for ( int i=0; i< frase.length(); i++){
            letra = frase.toLowerCase().charAt(i);
            if ( letra=='a' || letra=='e' || letra=='i' || letra=='o' || letra=='u')
                vocales++; 
        }
        System.out.println("Vocales en la  frase: " + vocales);
        
    }
    
}
